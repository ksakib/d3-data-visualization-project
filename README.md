# D3 Data Visualization Project

The project is a radial chart of my sleep time and the stages.
## Table of Content
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [D3 Data Visualization Project](#d3-data-visualization-project)
	- [Data](#data)
		- [Example of original csv file](#example-of-original-csv-file)
		- [Example of the extracted csv data](#example-of-the-extracted-csv-data)
		- [Example of the processed csv data](#example-of-the-processed-csv-data)
				- [The label for the stages:](#the-label-for-the-stages)
	- [Chart](#chart)
		- [Interaction](#interaction)
		- [Screenshots](#screenshots)
		- [observablehq link to check working chart](#observablehq-link-to-check-working-chart)
	- [Reference](#reference)
		- [library used](#library-used)
		- [Software used](#software-used)

<!-- /TOC -->

## Data
The data is collected by my Samsung Galaxy Watch Active 2 on the samsung health app. I used the data from February to October. The data collected is for 4 different sleep stages.

### Example of original csv file

| start_time | pkg_name | update_time | create_time | stage | time_offset | datauuid | sleep_id | custom | end_time | deviceuuid |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 2020-01-08 09:00:00.000 | com.sec.android.app.shealth | 2020-01-08 15:35:50.300 | 2020-01-08 15:35:50.300 | 40001 | UTC-0500 | 403f063e-c65a-e223-974a-50cf0aa94367 | 8d98c132-30ca-1d6b-6b8f-c17093d43f7d |  | 2020-01-08 09:02:00.000 | qMMK+kyTwW  |
| 2020-01-08 09:02:00.000 | com.sec.android.app.shealth | 2020-01-08 15:35:50.300 | 2020-01-08 15:35:50.300 | 40002 | UTC-0500 | 691c3b9b-b25e-74a1-5c49-7d5d428dbfd7 | 8d98c132-30ca-1d6b-6b8f-c17093d43f7d |  | 2020-01-08 09:08:00.000 | qMMK+kyTwW  |
| 2020-01-08 09:08:00.000 | com.sec.android.app.shealth | 2020-01-08 15:35:50.300 | 2020-01-08 15:35:50.300 | 40001 | UTC-0500 | 9df1fb93-9e3e-caf6-ed91-a42f30937bae | 8d98c132-30ca-1d6b-6b8f-c17093d43f7d |  | 2020-01-08 09:09:00.000 | qMMK+kyTwW |
| 2020-01-08 09:09:00.000 | com.sec.android.app.shealth | 2020-01-08 15:35:50.300 | 2020-01-08 15:35:50.300 | 40002 | UTC-0500 | 1feffa01-9651-b49b-7167-394bd3eaa2c4 | 8d98c132-30ca-1d6b-6b8f-c17093d43f7d |  | 2020-01-08 09:13:00.000 | qMMK+kyTwW |


### Example of the extracted csv data
| local_start_date | STAGE_AWAKE | STAGE_LIGHT | STAGE_DEEP | STAGE_REM |
| ------ | ------ | ------ | ------ | ------ |
| 43838 | 0.036805556 | 0.18125 | 0.002083333 | 0.052083333 |
| 43839 | 0.054166667 | 0.226388889 | 0.129861111 | 0.011805556 | 0.098611111 |
| 43840 | 0.033333333 | 0.129861111 | 0.088888889 | 0.010416667 | 0.048611111 |
| 43841 | 0.226388889 | 0.098611001 | 0.011805556 | 0.036805556 | 0.002083333 |

### Example of the processed csv data
| local_start_date | STAGE_AWAKE | STAGE_LIGHT | STAGE_DEEP | STAGE_REM |
| ------ | ------ | ------ | ------ | ------ |
| 02/01/20 | 47 | 139 | 29 | 35 |
| 02/02/20 | 15 | 94 | 0 | 26 |
| 02/03/20 | 38 | 161 | 11 | 72 |
| 02/04/20 | 105 | 456 | 62 | 150 |

##### The label for the stages:

1. **STAGE_REM** - In STAGE_REM, the user's body is relaxed and immobile while the user's brain becomes more active and re-energizes itself for the next day. This is the stage when the majority of the user's dreams occur.
2. **STAGE_DEEP** - In STAGE_DEEP, the user becomes less responsive to the user's surroundings. The user's breathing slows and his/her muscles are more relaxed. This is the stage when the user's body restores and recharges itself.
3. **STAGE_LIGHT** - In STAGE_LIGHT, the user transits from wakefulness to sleep. The user's breading and heart rate become regular and the user's body temperature drops.
4. **STAGE_AWAKE** - In STAGE_AWAKE includes the times you wake up briefly and the periods the user is restless during a sleep.

## Chart

The chart is represented in a radial way with four stages of sleep on each bars. Each stages are represented in differnt colors.

### Interaction
* **mouseover** - highlights the stage with stroke
* **click** - shows the time spent on that stage

### Screenshots

![SS of First part](./Radial%20Stacked%20Bar%20Chart-1.png)

![SS of Second part](https://git.ucsc.edu/ksakib/d3-data-visualization-project/-/blob/master/CMPM_D3_VIS_Pt2/Radial%20Stacked%20Bar%20Chart-1.png)

### observablehq link to check working chart
* **Part 1** -
* **Part 2** - https://observablehq.com/@sakib/sleepchart

## Reference

### library used
* Observablehq
* D3
* Samsung Health API

### Software used
* Atom
* LibreOffice
